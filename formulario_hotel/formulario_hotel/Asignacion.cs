﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace formulario_hotel
{
    public partial class Asignacion : Form
    {
        public Asignacion()
        {
            InitializeComponent();
        }

        public string parametro2;
        public int parametro3;
        public string parametro4;

        //Coneción de la base de datos
        private MySqlConnection conectarBD()
        {
            string cadenaConex = "datasource=127.0.0.1;port=3306;username=root;password=root;database=Hotel";
            MySqlConnection conec = new MySqlConnection(cadenaConex);
            return conec;
        }

        private void limpiar()
            {
                TxtFolio1.Clear();
                TxtAsignacion.Clear();
            }
         


    private void btnMostrarTodos1_Click(object sender, EventArgs e)
        {
            consultaGen();
        }

        private void consultaGen()
        {
            string query = "select * from Asignacion";
            MySqlConnection conectado2 = conectarBD();
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado2);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado2.Open();
                MySqlDataReader consulta = comandoSQL.ExecuteReader();
                //limpiar el grid
                dgAsigna.Rows.Clear();
                dgAsigna.Refresh();
                //validar si se encontraron datos
                if (consulta.HasRows)
                {
                    while (consulta.Read())
                    {
                        string[] filas = {
                            consulta.GetInt16(0).ToString(),
                            consulta.GetString(1),
                            consulta.GetInt16(2).ToString()
                        };
                        dgAsigna.Rows.Add(filas);
                    }
                }
                conectado2.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }
        }

        private void btnBuscar1_Click(object sender, EventArgs e)
        {
            buscarUno();
        }

        private void buscarUno()
        {
            MySqlConnection conectado2 = conectarBD();
            string query = "select * from Asignacion where id_asigna=" + TxtAsignacion.Text;
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado2);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado2.Open();
                MySqlDataReader result = comandoSQL.ExecuteReader();
                //si existen datos
                if (result.HasRows)
                {
                    while (result.Read())
                    {
                        //extraemos los valores de la consulta
                        TxtFolio1.Text = result.GetString(1);
                        comboHab.Text = result.GetString(2);
                        
                }
                }
                else
                {
                    MessageBox.Show("No se encuentra el registro");
                }
                conectado2.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error: " + err.ToString());
            }
        }

        private void btnEliminar1_Click(object sender, EventArgs e)
        {
            MySqlConnection conecta2 = conectarBD();
            string query = "delete from Asignacion where id_asigna = " + TxtAsignacion.Text;
            DialogResult dialog0Result = MessageBox.Show("¿Confirma que desea eliminar?", "Advertencia", MessageBoxButtons.YesNo);
            if (dialog0Result == DialogResult.Yes)
            {
                MySqlCommand comando = new MySqlCommand(query, conecta2);
                comando.CommandTimeout = 60;
                try
                {
                    conecta2.Open();
                    MySqlDataReader result = comando.ExecuteReader();
                    consultaGen();
                    limpiar();
                    MessageBox.Show("Registro eliminado");
                    conecta2.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error: " + err.ToString());
                }
            }
        }

        private void BtnActualizar1_Click(object sender, EventArgs e)
        {
            actualizar();
        }

        private void actualizar()
        {
            MySqlConnection conectado2 = conectarBD();
            string query = "update Asignacion set folio = '" + TxtFolio1.Text + "', num_habitacion = " + comboHab.Text + " where id_asigna = " + TxtAsignacion.Text;
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado2);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado2.Open();
                MySqlDataReader result = comandoSQL.ExecuteReader();
                MessageBox.Show("Registro actualizado");
                consultaGen();
                limpiar();
                conectado2.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }
        }

        private void btnRegistrar1_Click(object sender, EventArgs e)
        {
            if (TxtAsignacion.Text == "")
            {
                MessageBox.Show("Debe de ingresar el número de la asiganción");
            }
            if (TxtFolio1.Text == "")
            {
                MessageBox.Show("Debe de ingresar el número del folio de la reservación");
            }
            if (comboHab.Text == "")
            {
                   MessageBox.Show("Debe de ingresar el número de la habitación que se asignara");
            }
            else
            {
                string query = "insert into Asignacion values(" + TxtAsignacion.Text + ", '" + TxtFolio1.Text + "', " + comboHab.Text + ")";
                MySqlConnection conectado2 = conectarBD();
                MySqlCommand comandoSQL1 = new MySqlCommand(query, conectado2);
                //opcional
                comandoSQL1.CommandTimeout = 60;
                try
                {
                    //se abre la conexión
                    conectado2.Open();
                    //ejecutar el comando SQL
                    MySqlDataReader ejecutaSQL = comandoSQL1.ExecuteReader();
                    MessageBox.Show("Datos de la asignación insertados");
                    limpiar();
                    consultaGen();
                    //importante cerrar la conexión
                    conectado2.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error:" + err.ToString());
                }
            }
            /*else
            {
                
                MySqlConnection conectado1 = conectarBD();
                string query = "Select estatus from Habitacion where num_habitacion = " + comboHab.Text + "";
                MySqlCommand comandoSQL1 = new MySqlCommand(query, conectado1);
                comandoSQL1.CommandTimeout = 60;
                try
                {
                    conectado1.Open();
                    MySqlDataReader result = comandoSQL1.ExecuteReader();
                    //si existen datos
                    if (result.Equals("Libre"))
                    {
                        while (result.Read())
                        {
                            //estatus.Text = result.GetString(0);
                        }

                        MySqlConnection conectado3 = conectarBD();
                        string queryH = "Update Habitacion set estatus = "+""Ocupada""+" where num_habitacion = " + comboHab.Text + "";
                        MySqlCommand comandoSQL = new MySqlCommand(queryH, conectado3);
                        comandoSQL.CommandTimeout = 60;
                        try
                        {
                            conectado3.Open();
                            MySqlDataReader result1 = comandoSQL.ExecuteReader();
                            MessageBox.Show("Estatus actualizado");
                            consultaGen();
                            conectado3.Close();

                            string queryA = "insert into Asignacion values(" + TxtAsignacion.Text + ", '" + TxtFolio1.Text + "', " + comboHab.Text + ")";
                            MySqlConnection conectado2 = conectarBD();
                            MySqlCommand comandoSQL2 = new MySqlCommand(queryA, conectado2);
                            //opcional
                            comandoSQL2.CommandTimeout = 60;
                            try
                            {
                                //se abre la conexión
                                conectado2.Open();
                                //ejecutar el comando SQL
                                MySqlDataReader ejecutaSQL = comandoSQL2.ExecuteReader();
                                MessageBox.Show("Datos de la asignación insertados");
                                limpiar();
                                consultaGen();
                                //importante cerrar la conexión
                                conectado2.Close();
                            }
                            catch (Exception err)
                            {
                                MessageBox.Show("Error:" + err.ToString());
                            }
                        }
                        catch (Exception err)
                        {
                            MessageBox.Show("Error:" + err.ToString());
                        }
                        limpiar();
                    }
                    else
                    {
                        MessageBox.Show("No se puede realizar la asiganción");
                    }
                    conectado1.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error: " + err.ToString());
                }
            }*/
       

           /* MySqlConnection conectado1 = conectarBD();
            string query = "Select estatus from Habitacion where num_habitacion = " + comboHab.Text + "";
            MySqlCommand comandoSQL1 = new MySqlCommand(query, conectado1);
            comandoSQL1.CommandTimeout = 60;
            try
            {
                conectado1.Open();
                MySqlDataReader result = comandoSQL1.ExecuteReader();
                //si existen datos
                if (result.Equals("Libre"))
                {
                    while (result.Read())
                    {
                        estatus.Text = result.GetString(0);
                    }

                    MySqlConnection conectado1 = conectarBD();
                    string query = "Update Habitacion set estatus = " + "Ocupada" + " where num_habitacion = " + comboHab.Text + "";
                    MySqlCommand comandoSQL = new MySqlCommand(query, conectado1);
                    comandoSQL.CommandTimeout = 60;
                    try
                    {
                        conectado1.Open();
                        MySqlDataReader result = comandoSQL.ExecuteReader();
                        MessageBox.Show("Estatus actualizado");
                        consultaGen();
                        //limpiar();
                        conectado1.Close();
                    }
                    catch (Exception err)
                    {
                        MessageBox.Show("Error:" + err.ToString());
                    }
                    limpiar();
                }
                else
                {
                    MessageBox.Show("No se puede realizar la asiganción");
                }
                conectado1.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error: " + err.ToString());
            }*/
        }

           /* MySqlConnection conectado2 = conectarBD();
            string queryEstatus = "Select estatus from Habitacion where num_habitacion = " + comboHab.Text + "";
            MySqlCommand comandoSQL1 = new MySqlCommand(queryEstatus, conectado22);
            comandoSQL1.CommandTimeout = 60;
            try
            {
                conectado22.Open();
                MySqlDataReader result = comandoSQL1.ExecuteReader();
                //si existen datos
                if (result.Equals("Libre"))
                {
                    while (result.Read())
                    {
                        MessageBox.Show("HABITACION LIBRE");
                    }
                }
                else
                {
                    MessageBox.Show("No se encuentra el registro");
                }
                conectado22.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error: " + err.ToString());
            }*/

            //funciona
            /*string query = "insert into Asignacion values(" + TxtAsignacion.Text + ", '" + TxtFolio1.Text + "', " + comboHab.Text + ")";
            MySqlConnection conectado2 = conectarBD();
            MySqlCommand comandoSQL1 = new MySqlCommand(query, conectado2);
            //opcional
            comandoSQL1.CommandTimeout = 60;
            try
            {
                //se abre la conexión
                conectado2.Open();
                //ejecutar el comando SQL
                MySqlDataReader ejecutaSQL = comandoSQL1.ExecuteReader();
                MessageBox.Show("Datos de la asignación insertados");
                limpiar();
                consultaGen();
                //importante cerrar la conexión
                conectado2.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }*/
    }
}

