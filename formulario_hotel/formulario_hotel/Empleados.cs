﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Referencia a la base de datos
using MySql.Data.MySqlClient;

namespace formulario_hotel
{
    public partial class Empleados : Form
    {
        public Empleados()
        {
            InitializeComponent();
        }

        //Coneción de la base de datos
        private MySqlConnection conectarBD()
        {
            string cadenaConex = "datasource=127.0.0.1;port=3306;username=root;password=root;database=Hotel";
            MySqlConnection conec = new MySqlConnection(cadenaConex);
            return conec;
        }

        //Limpiar todas las cajas de texto
        private void limpiar()
        {
            txtIdEmp.Clear();
            txtNombre.Clear();
            txtCargo.Clear();
        }

        private void BtnMost_Click(object sender, EventArgs e)
        {
            consultaGen();
        }

        private void consultaGen()
        {
            string query = "select * from Empleado";
            MySqlConnection conectado = conectarBD();
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado.Open();
                MySqlDataReader consulta = comandoSQL.ExecuteReader();
                //limpiar el grid
                dgEmp.Rows.Clear();
                dgEmp.Refresh();
                //validar si se encontraron datos
                if (consulta.HasRows)
                {
                    while (consulta.Read())
                    {
                        string[] filas = {consulta.GetInt32(0).ToString(),
                            consulta.GetString(1),
                            consulta.GetString(2)
                        };
                        dgEmp.Rows.Add(filas);
                    }
                }
                conectado.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtIdEmp.Text == "")
            {
                MessageBox.Show("Debe de ingresar un Id para el empleado");
            }
            if (txtNombre.Text == "")
            {
                MessageBox.Show("Ingrese el nombre del empleado");
            }
            if (txtCargo.Text == "")
            {
                MessageBox.Show("Ingrese el cargo del empleado");
            }
            else
            {
                Form1 id = new Form1();
                id.parametro1 = int.Parse(txtIdEmp.Text);

                string queryR = "insert into Empleado values(" + txtIdEmp.Text + ", '" + txtNombre.Text + "', '" + txtCargo.Text + "')";
                MySqlConnection conectado1 = conectarBD();
                MySqlCommand comandoSQL = new MySqlCommand(queryR, conectado1);
                //opcional
                comandoSQL.CommandTimeout = 60;
                try
                {
                    //se abre la conexión
                    conectado1.Open();
                    //ejecutar el comando SQL
                    MySqlDataReader ejecutaSQL = comandoSQL.ExecuteReader();
                    MessageBox.Show("Datos del empleado insertados");
                    limpiar();
                    consultaGen();
                    //importante cerrar la conexión
                    conectado1.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error:" + err.ToString());
                }
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            actualizar();
        }

        private void actualizar()
        {
            MySqlConnection conectado1 = conectarBD();
            string query = "update Empleado set nombre_empleado = '" + txtNombre.Text + "', cargo = '" + txtCargo.Text + "' where id_empleado = " + txtIdEmp.Text;
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado1);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado1.Open();
                MySqlDataReader result = comandoSQL.ExecuteReader();
                MessageBox.Show("Registro del empleado actualizado");
                consultaGen();
                limpiar();
                conectado1.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            MySqlConnection conecta1 = conectarBD();
            string query = "delete from Empleado where id_empleado = " + txtIdEmp.Text;
            DialogResult dialog0Result = MessageBox.Show("¿Confirma que desea eliminar?", "Advertencia", MessageBoxButtons.YesNo);
            if (dialog0Result == DialogResult.Yes)
            {
                MySqlCommand comando = new MySqlCommand(query, conecta1);
                comando.CommandTimeout = 60;
                try
                {
                    conecta1.Open();
                    MySqlDataReader result = comando.ExecuteReader();
                    consultaGen();
                    limpiar();
                    MessageBox.Show("Registro del empleado eliminado");
                    conecta1.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error: " + err.ToString());
                }
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            buscarUno();
        }

        private void buscarUno()
        {
            MySqlConnection conectado = conectarBD();
            string query = "select * from Empleado where id_empleado =" + txtIdEmp.Text;
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado.Open();
                MySqlDataReader result = comandoSQL.ExecuteReader();
                //si existen datos
                if (result.HasRows)
                {
                    while (result.Read())
                    {
                        //extraemos los valores de la consulta
                        txtNombre.Text = result.GetString(1);
                        txtCargo.Text = result.GetString(2);
                    }
                }
                else
                {
                    MessageBox.Show("No se encuentra el registro");
                }
                conectado.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error: " + err.ToString());
            }
        }

    }
}
