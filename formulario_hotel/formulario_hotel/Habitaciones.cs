﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Referencia a la base de datos
using MySql.Data.MySqlClient;

namespace formulario_hotel
{
    public partial class Habitaciones : Form
    {
        public Habitaciones()
        {
            InitializeComponent();
        }

        //Coneción de la base de datos
        private MySqlConnection conectarBD()
        {
            string cadenaConex = "datasource=127.0.0.1;port=3306;username=root;password=root;database=Hotel";
            MySqlConnection conec = new MySqlConnection(cadenaConex);
            return conec;
        }

        //Limpiar todas las cajas de texto
        private void limpiar()
        {
            txtnum.Clear();
            txtTip.SelectedIndex = 0;
            txtCap.Clear();
            txtEstatus.SelectedIndex = 0;
        }

        private void BtnMost1_Click(object sender, EventArgs e)
        {
            consultaGen();
        }

        private void consultaGen()
        {
            string query = "select * from Habitacion";
            MySqlConnection conectado = conectarBD();
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado.Open();
                MySqlDataReader consulta = comandoSQL.ExecuteReader();
                //limpiar el grid
                dgHab.Rows.Clear();
                dgHab.Refresh();
                //validar si se encontraron datos
                if (consulta.HasRows)
                {
                    while (consulta.Read())
                    {
                        string[] filas = {
                            consulta.GetInt32(0).ToString(),
                            consulta.GetString(1),
                            consulta.GetInt32(2).ToString(),
                            consulta.GetString(3)
                        };
                        dgHab.Rows.Add(filas);
                    }
                }
                conectado.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            buscarUno();
        }

        private void buscarUno()
        {
            MySqlConnection conectado = conectarBD();
            string query = "select * from Habitacion where num_habitacion =" + txtnum.Text;
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado.Open();
                MySqlDataReader result = comandoSQL.ExecuteReader();
                //si existen datos
                if (result.HasRows)
                {
                    while (result.Read())
                    {
                        //extraemos los valores de la consulta
                        txtTip.Text = result.GetString(1);
                        txtCap.Text = result.GetString(2);
                        txtEstatus.Text = result.GetString(3);
                    }
                }
                else
                {
                    MessageBox.Show("No se encuentra el registro");
                }
                conectado.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error: " + err.ToString());
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            MySqlConnection conecta1 = conectarBD();
            string query = "delete from Habitacion where num_habitacion = " + txtnum.Text;
            DialogResult dialog0Result = MessageBox.Show("¿Confirma que desea eliminar?", "Advertencia", MessageBoxButtons.YesNo);
            if (dialog0Result == DialogResult.Yes)
            {
                MySqlCommand comando = new MySqlCommand(query, conecta1);
                comando.CommandTimeout = 60;
                try
                {
                    conecta1.Open();
                    MySqlDataReader result = comando.ExecuteReader();
                    consultaGen();
                    limpiar();
                    MessageBox.Show("Registro eliminado");
                    conecta1.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error: " + err.ToString());
                }
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            actualizar();
        }

        private void actualizar()
        {
            MySqlConnection conectado1 = conectarBD();
            string query = "update Habitacion set tipo_hab = '" + txtTip.Text + "', capacidad_hab = " + txtCap.Text + ", estatus = '" + txtEstatus.Text + "' where num_habitacion = " + txtnum.Text;
            MySqlCommand comandoSQL9 = new MySqlCommand(query, conectado1);
            comandoSQL9.CommandTimeout = 60;
            try
            {
                conectado1.Open();
                MySqlDataReader result = comandoSQL9.ExecuteReader();
                MessageBox.Show("Registro de la habitación actualizado");
                consultaGen();
                limpiar();
                conectado1.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtnum.Text == "")
            {
                MessageBox.Show("Debe de ingresar el número de la habitación");
            }
            if (txtTip.Text == "")
            {
                MessageBox.Show("Debe de seleccionar un tipo de habitación");
            }
            if (txtCap.Text == "")
            {
                MessageBox.Show("Ingrese la capacidad de la habitación");
            }
            if (txtEstatus.Text == "")
            {
                MessageBox.Show("Ingrese el estatus de la habitación");
            }
            else
            {
                Asignacion est = new Asignacion();
                est.parametro4 = txtEstatus.Text;

                string queryR = "insert into Habitacion values(" + txtnum.Text + ", '" + txtTip.Text + "', " + txtCap.Text + " , '" + txtEstatus.Text + "')";

                MySqlConnection conectado1 = conectarBD();
                MySqlCommand comandoSQL1 = new MySqlCommand(queryR, conectado1);
                //opcional
                comandoSQL1.CommandTimeout = 60;
                try
                {
                    //se abre la conexión
                    conectado1.Open();
                    //ejecutar el comando SQL
                    MySqlDataReader ejecutaSQL = comandoSQL1.ExecuteReader();
                    MessageBox.Show("Datos de la reservación insertados");
                    limpiar();
                    consultaGen();
                    //importante cerrar la conexión
                    conectado1.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error:" + err.ToString());
                }

            }
            //Asignacion num = new Asignacion();
            //num.parametro3 = int.Parse(txtnum.Text);
            //num.Show();
        }
    }
}
