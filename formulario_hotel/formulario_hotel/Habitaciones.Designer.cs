﻿namespace formulario_hotel
{
    partial class Habitaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Habitaciones));
            this.label1 = new System.Windows.Forms.Label();
            this.dgHab = new System.Windows.Forms.DataGridView();
            this.num_habitacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipo_habitacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.capacidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnMost1 = new System.Windows.Forms.Button();
            this.PbImagen = new System.Windows.Forms.PictureBox();
            this.txtCap = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtnum = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.BtnActualizar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTip = new System.Windows.Forms.ComboBox();
            this.txtEstatus = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgHab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbImagen)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(206, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(377, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Información de las Habitaciones";
            // 
            // dgHab
            // 
            this.dgHab.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgHab.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.num_habitacion,
            this.tipo_habitacion,
            this.capacidad,
            this.estatus});
            this.dgHab.Location = new System.Drawing.Point(12, 166);
            this.dgHab.Name = "dgHab";
            this.dgHab.Size = new System.Drawing.Size(571, 120);
            this.dgHab.TabIndex = 2;
            // 
            // num_habitacion
            // 
            this.num_habitacion.HeaderText = "Núm. Habitación";
            this.num_habitacion.Name = "num_habitacion";
            // 
            // tipo_habitacion
            // 
            this.tipo_habitacion.HeaderText = "Tipo";
            this.tipo_habitacion.Name = "tipo_habitacion";
            // 
            // capacidad
            // 
            this.capacidad.HeaderText = "Capacidad";
            this.capacidad.Name = "capacidad";
            // 
            // estatus
            // 
            this.estatus.HeaderText = "Estatus";
            this.estatus.Name = "estatus";
            // 
            // BtnMost1
            // 
            this.BtnMost1.Location = new System.Drawing.Point(18, 74);
            this.BtnMost1.Name = "BtnMost1";
            this.BtnMost1.Size = new System.Drawing.Size(129, 23);
            this.BtnMost1.TabIndex = 5;
            this.BtnMost1.Text = "&Mostrar Todos";
            this.BtnMost1.UseVisualStyleBackColor = true;
            this.BtnMost1.Click += new System.EventHandler(this.BtnMost1_Click);
            // 
            // PbImagen
            // 
            this.PbImagen.Image = ((System.Drawing.Image)(resources.GetObject("PbImagen.Image")));
            this.PbImagen.Location = new System.Drawing.Point(12, 25);
            this.PbImagen.Name = "PbImagen";
            this.PbImagen.Size = new System.Drawing.Size(171, 119);
            this.PbImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbImagen.TabIndex = 6;
            this.PbImagen.TabStop = false;
            // 
            // txtCap
            // 
            this.txtCap.Location = new System.Drawing.Point(272, 111);
            this.txtCap.Name = "txtCap";
            this.txtCap.Size = new System.Drawing.Size(108, 20);
            this.txtCap.TabIndex = 43;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(208, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "Capacidad";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(208, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Tipo";
            // 
            // txtnum
            // 
            this.txtnum.Location = new System.Drawing.Point(315, 57);
            this.txtnum.Name = "txtnum";
            this.txtnum.Size = new System.Drawing.Size(65, 20);
            this.txtnum.TabIndex = 39;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnBuscar);
            this.groupBox2.Controls.Add(this.BtnActualizar);
            this.groupBox2.Controls.Add(this.btnEliminar);
            this.groupBox2.Controls.Add(this.btnRegistrar);
            this.groupBox2.Controls.Add(this.BtnMost1);
            this.groupBox2.Location = new System.Drawing.Point(413, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(170, 103);
            this.groupBox2.TabIndex = 38;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Seleccionar";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(87, 45);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 17;
            this.btnBuscar.Text = "&Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // BtnActualizar
            // 
            this.BtnActualizar.Location = new System.Drawing.Point(87, 16);
            this.BtnActualizar.Name = "BtnActualizar";
            this.BtnActualizar.Size = new System.Drawing.Size(75, 23);
            this.BtnActualizar.TabIndex = 16;
            this.BtnActualizar.Text = "&Actualizar";
            this.BtnActualizar.UseVisualStyleBackColor = true;
            this.BtnActualizar.Click += new System.EventHandler(this.BtnActualizar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(6, 45);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar.TabIndex = 15;
            this.btnEliminar.Text = "&Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(6, 16);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrar.TabIndex = 13;
            this.btnRegistrar.Text = "&Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(208, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Núm. de Habitación";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(211, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 44;
            this.label5.Text = "Estatus";
            // 
            // txtTip
            // 
            this.txtTip.FormattingEnabled = true;
            this.txtTip.Items.AddRange(new object[] {
            "",
            "Sencilla",
            "Doble",
            "Suite"});
            this.txtTip.Location = new System.Drawing.Point(242, 84);
            this.txtTip.Name = "txtTip";
            this.txtTip.Size = new System.Drawing.Size(138, 21);
            this.txtTip.TabIndex = 46;
            // 
            // txtEstatus
            // 
            this.txtEstatus.FormattingEnabled = true;
            this.txtEstatus.Items.AddRange(new object[] {
            "",
            "Libre",
            "Ocupada",
            "En mantenimiento"});
            this.txtEstatus.Location = new System.Drawing.Point(259, 139);
            this.txtEstatus.Name = "txtEstatus";
            this.txtEstatus.Size = new System.Drawing.Size(121, 21);
            this.txtEstatus.TabIndex = 47;
            // 
            // Habitaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 298);
            this.Controls.Add(this.txtEstatus);
            this.Controls.Add(this.txtTip);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCap);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtnum);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PbImagen);
            this.Controls.Add(this.dgHab);
            this.Controls.Add(this.label1);
            this.Name = "Habitaciones";
            this.Text = "Habitaciones";
            ((System.ComponentModel.ISupportInitialize)(this.dgHab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbImagen)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgHab;
        private System.Windows.Forms.Button BtnMost1;
        private System.Windows.Forms.PictureBox PbImagen;
        private System.Windows.Forms.DataGridViewTextBoxColumn num_habitacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipo_habitacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn capacidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn estatus;
        private System.Windows.Forms.TextBox txtCap;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtnum;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button BtnActualizar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox txtTip;
        private System.Windows.Forms.ComboBox txtEstatus;
    }
}