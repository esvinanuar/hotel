﻿namespace formulario_hotel
{
    partial class Asignacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Asignacion));
            this.label1 = new System.Windows.Forms.Label();
            this.PbImagen = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dgAsigna = new System.Windows.Forms.DataGridView();
            this.id_asignacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folio1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.num_habitacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnMostrarTodos1 = new System.Windows.Forms.Button();
            this.btnBuscar1 = new System.Windows.Forms.Button();
            this.BtnActualizar1 = new System.Windows.Forms.Button();
            this.btnEliminar1 = new System.Windows.Forms.Button();
            this.btnRegistrar1 = new System.Windows.Forms.Button();
            this.TxtAsignacion = new System.Windows.Forms.TextBox();
            this.TxtFolio1 = new System.Windows.Forms.TextBox();
            this.comboHab = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.PbImagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAsigna)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(210, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 58);
            this.label1.TabIndex = 0;
            this.label1.Text = "Asiganación de las\r\n    habitaciones\r\n";
            // 
            // PbImagen
            // 
            this.PbImagen.Image = ((System.Drawing.Image)(resources.GetObject("PbImagen.Image")));
            this.PbImagen.Location = new System.Drawing.Point(12, 12);
            this.PbImagen.Name = "PbImagen";
            this.PbImagen.Size = new System.Drawing.Size(172, 119);
            this.PbImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbImagen.TabIndex = 4;
            this.PbImagen.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Folio";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(305, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Núm. habitacion";
            // 
            // dgAsigna
            // 
            this.dgAsigna.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAsigna.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_asignacion,
            this.folio1,
            this.num_habitacion});
            this.dgAsigna.Location = new System.Drawing.Point(13, 194);
            this.dgAsigna.Name = "dgAsigna";
            this.dgAsigna.Size = new System.Drawing.Size(438, 116);
            this.dgAsigna.TabIndex = 25;
            // 
            // id_asignacion
            // 
            this.id_asignacion.HeaderText = "Id Asignación";
            this.id_asignacion.Name = "id_asignacion";
            // 
            // folio1
            // 
            this.folio1.HeaderText = "Folio";
            this.folio1.Name = "folio1";
            // 
            // num_habitacion
            // 
            this.num_habitacion.HeaderText = "Núm. de habitación";
            this.num_habitacion.Name = "num_habitacion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(246, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Id asignación:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnMostrarTodos1);
            this.groupBox1.Controls.Add(this.btnBuscar1);
            this.groupBox1.Controls.Add(this.BtnActualizar1);
            this.groupBox1.Controls.Add(this.btnEliminar1);
            this.groupBox1.Controls.Add(this.btnRegistrar1);
            this.groupBox1.Location = new System.Drawing.Point(12, 143);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 45);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccionar";
            // 
            // btnMostrarTodos1
            // 
            this.btnMostrarTodos1.Location = new System.Drawing.Point(329, 16);
            this.btnMostrarTodos1.Name = "btnMostrarTodos1";
            this.btnMostrarTodos1.Size = new System.Drawing.Size(104, 23);
            this.btnMostrarTodos1.TabIndex = 18;
            this.btnMostrarTodos1.Text = "&Mostrar Todos";
            this.btnMostrarTodos1.UseVisualStyleBackColor = true;
            this.btnMostrarTodos1.Click += new System.EventHandler(this.btnMostrarTodos1_Click);
            // 
            // btnBuscar1
            // 
            this.btnBuscar1.Location = new System.Drawing.Point(249, 16);
            this.btnBuscar1.Name = "btnBuscar1";
            this.btnBuscar1.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar1.TabIndex = 17;
            this.btnBuscar1.Text = "&Buscar";
            this.btnBuscar1.UseVisualStyleBackColor = true;
            this.btnBuscar1.Click += new System.EventHandler(this.btnBuscar1_Click);
            // 
            // BtnActualizar1
            // 
            this.BtnActualizar1.Location = new System.Drawing.Point(87, 16);
            this.BtnActualizar1.Name = "BtnActualizar1";
            this.BtnActualizar1.Size = new System.Drawing.Size(75, 23);
            this.BtnActualizar1.TabIndex = 16;
            this.BtnActualizar1.Text = "&Actualizar";
            this.BtnActualizar1.UseVisualStyleBackColor = true;
            this.BtnActualizar1.Click += new System.EventHandler(this.BtnActualizar1_Click);
            // 
            // btnEliminar1
            // 
            this.btnEliminar1.Location = new System.Drawing.Point(168, 16);
            this.btnEliminar1.Name = "btnEliminar1";
            this.btnEliminar1.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar1.TabIndex = 15;
            this.btnEliminar1.Text = "&Eliminar";
            this.btnEliminar1.UseVisualStyleBackColor = true;
            this.btnEliminar1.Click += new System.EventHandler(this.btnEliminar1_Click);
            // 
            // btnRegistrar1
            // 
            this.btnRegistrar1.Location = new System.Drawing.Point(6, 16);
            this.btnRegistrar1.Name = "btnRegistrar1";
            this.btnRegistrar1.Size = new System.Drawing.Size(75, 23);
            this.btnRegistrar1.TabIndex = 13;
            this.btnRegistrar1.Text = "&Registrar";
            this.btnRegistrar1.UseVisualStyleBackColor = true;
            this.btnRegistrar1.Click += new System.EventHandler(this.btnRegistrar1_Click);
            // 
            // TxtAsignacion
            // 
            this.TxtAsignacion.Location = new System.Drawing.Point(323, 108);
            this.TxtAsignacion.Name = "TxtAsignacion";
            this.TxtAsignacion.Size = new System.Drawing.Size(66, 20);
            this.TxtAsignacion.TabIndex = 37;
            // 
            // TxtFolio1
            // 
            this.TxtFolio1.Location = new System.Drawing.Point(229, 81);
            this.TxtFolio1.Name = "TxtFolio1";
            this.TxtFolio1.Size = new System.Drawing.Size(66, 20);
            this.TxtFolio1.TabIndex = 6;
            // 
            // comboHab
            // 
            this.comboHab.Location = new System.Drawing.Point(395, 81);
            this.comboHab.Name = "comboHab";
            this.comboHab.Size = new System.Drawing.Size(56, 20);
            this.comboHab.TabIndex = 38;
            // 
            // Asignacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 322);
            this.Controls.Add(this.comboHab);
            this.Controls.Add(this.TxtAsignacion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dgAsigna);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TxtFolio1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PbImagen);
            this.Controls.Add(this.label1);
            this.Name = "Asignacion";
            this.Text = "Asignación";
            ((System.ComponentModel.ISupportInitialize)(this.PbImagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAsigna)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox PbImagen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dgAsigna;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnEliminar1;
        private System.Windows.Forms.Button btnRegistrar1;
        private System.Windows.Forms.Button BtnActualizar1;
        private System.Windows.Forms.Button btnBuscar1;
        private System.Windows.Forms.Button btnMostrarTodos1;
        private System.Windows.Forms.TextBox TxtAsignacion;
        private System.Windows.Forms.TextBox TxtFolio1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_asignacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn folio1;
        private System.Windows.Forms.DataGridViewTextBoxColumn num_habitacion;
        private System.Windows.Forms.TextBox comboHab;
    }
}