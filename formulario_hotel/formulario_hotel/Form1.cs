﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//Referencia a la base de datos
using MySql.Data.MySqlClient;


namespace formulario_hotel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int parametro1;
        public string nom;

        //Coneción de la base de datos
        private MySqlConnection conectarBD()
        {
            string cadenaConex = "datasource=127.0.0.1;port=3306;username=root;password=root;database=Hotel";
            MySqlConnection conec = new MySqlConnection(cadenaConex);
            return conec;
        }

        //Limpiar todas las cajas de texto
        private void limpiar()
        {
            TxtFoil.Clear();
            TxtNombre.Clear();
            TxtOrigen.Clear();
            TxtTelefono.Clear();
            TxtCorreo.Clear();
            comboHrs.SelectedIndex = 0;
            TxtId.Clear();
            //comboEmp.Clear();
            //nombre.
        }

        private void asignacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Asignacion asigna = new Asignacion();
            asigna.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Empleados emp = new Empleados();
            emp.Show();
        }

        private void habitacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Habitaciones hab = new Habitaciones();
            hab.Show();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (TxtFoil.Text == "")
            {
                MessageBox.Show("Debe de ingresar el número de folio");
            }
            if (TxtId.Text == "")
            {
                MessageBox.Show("Debe de ingresar el Id del Huesped");
            }
            if (TxtNombre.Text == "")
            {
                MessageBox.Show("Debe de ingresar el nombre del huesped");
            }
            if (comboEmp.Text == "")
            {
                MessageBox.Show("Debes de ingresar el número de tú Id de empleado");
            }
            if (TxtCorreo.Text == "")
            {
                MessageBox.Show("Debes de ingresar el correo del huesped");
            }
            else 
            {
                Asignacion folio = new Asignacion();
                folio.parametro2 = TxtFoil.Text;
                //folio.Show();

                string query = "insert into Huesped values('" + TxtId.Text + "', '" + TxtNombre.Text + "', '" + TxtTelefono.Text + "', '" + TxtCorreo.Text + "', '" + TxtOrigen.Text + "')";
                MySqlConnection conectado = conectarBD();
                MySqlCommand comandoSQL2 = new MySqlCommand(query, conectado);
                //opcional
                comandoSQL2.CommandTimeout = 60;
                try
                {
                    //se abre la conexión
                    conectado.Open();
                    //ejecutar el comando SQL
                    MySqlDataReader ejecutaSQL = comandoSQL2.ExecuteReader();
                    MessageBox.Show("Datos del Huesped insertados");
                    consultaGen();
                    //importante cerrar la conexión
                    conectado.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error:" + err.ToString());
                }

                DateTime fecha1 = DateTime.Now;
                string dateFormatted = fecha1.ToString("yyyy-MM-dd");
                DateTime fecha22 = fecha2.Value;
                string dateFormatted1 = fecha22.ToString("yyyy-MM-dd");
                DateTime fecha33 = fecha3.Value;
                string dateFormatted2 = fecha33.ToString("yyyy-MM-dd");


                string queryR = "insert into Reservacion values('" + TxtFoil.Text + "', " + TxtId.Text + ", " + comboEmp.Text + ", '" + dateFormatted + "', '" + comboHrs.Text + "', '" + dateFormatted1 + "', '" + dateFormatted2 + "')";
                Console.WriteLine(queryR);
                MySqlConnection conectado2 = conectarBD();
                MySqlCommand comandoSQL8 = new MySqlCommand(queryR, conectado2);

                //opcional
                comandoSQL8.CommandTimeout = 60;
                try
                {
                    //se abre la conexión
                    conectado2.Open();
                    //ejecutar el comando SQL
                    MySqlDataReader ejecutaSQL = comandoSQL8.ExecuteReader();
                    MessageBox.Show("Datos de la reservación insertados");

                    consultaGen();
                    //importante cerrar la conexión
                    conectado2.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error:" + err.ToString());
                }
                limpiar();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            buscarUno();
        }

        private void buscarUno()
        {
            DateTime fecha11 = DateTime.Now;
            string dateFormatted = fecha11.ToString("yyyy-MM-dd");
            DateTime fecha22 = fecha2.Value;
            string dateFormatted1 = fecha22.ToString("yyyy-MM-dd");
            DateTime fecha33 = fecha3.Value;
            string dateFormatted2 = fecha33.ToString("yyyy-MM-dd");

            MySqlConnection conectado1 = conectarBD();
            string query = "select * from Reservacion where folio = '" + TxtFoil.Text +"'";
            MySqlCommand comandoSQL1 = new MySqlCommand(query, conectado1);
            comandoSQL1.CommandTimeout = 60;
            try
            {
                conectado1.Open();
                MySqlDataReader result = comandoSQL1.ExecuteReader();
                //si existen datos
                if (result.HasRows)
                {
                    while (result.Read())
                    {
                        //extraemos los valores de la consulta
                        //ERROR AL MOSTRAR LOS DATOS DE LA BUSQUEDA
                        TxtId.Text = result.GetInt16(1).ToString();
                        comboEmp.Text = result.GetInt16(2).ToString();
                        fecha1.Value = Convert.ToDateTime(result.GetString(3));
                        comboHrs.Text = result.GetString(4);
                        fecha2.Value = Convert.ToDateTime(result.GetString(5));
                        fecha3.Value = Convert.ToDateTime(result.GetString(6));
                    }
                }
                else
                {
                    MessageBox.Show("No se encuentra el registro de la reservación");
                }
                conectado1.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error: " + err.ToString());
            }

            MySqlConnection conectado2 = conectarBD();
            string queryH = "select * from Huesped where id_huesped = '" + TxtId.Text + "'";
            MySqlCommand comandoSQL3 = new MySqlCommand(queryH, conectado1);
            comandoSQL3.CommandTimeout = 60;
            try
            {
                conectado1.Open();
                MySqlDataReader result = comandoSQL3.ExecuteReader();
                //si existen datos
                if (result.HasRows)
                {
                    while (result.Read())
                    {
                        //extraemos los valores de la consulta
                        TxtId.Text = result.GetInt16(0).ToString();
                        TxtNombre.Text = result.GetString(1);
                        TxtTelefono.Text = result.GetString(2);
                        TxtCorreo.Text = result.GetString(3);
                        TxtOrigen.Text = result.GetString(4);   
                    }
                }
                else
                {
                    MessageBox.Show("No se encuentra el registro del huesped");
                }
                conectado1.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error: " + err.ToString());
            }
        }

        private void btnMostrarTodos_Click(object sender, EventArgs e)
        {
            consultaGen();
        }

        private void consultaGen()
        {
            string query = "select * from Reservacion";
            MySqlConnection conectado1 = conectarBD();
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado1);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado1.Open();
                MySqlDataReader consulta = comandoSQL.ExecuteReader();
                //limpiar el grid
                dgRes.Rows.Clear();
                dgRes.Refresh();
                //validar si se encontraron datos
                if (consulta.HasRows)
                {
                    while (consulta.Read())
                    {
                        string[] filas = {
                            consulta.GetString(0),
                            consulta.GetInt16(1).ToString(),
                            consulta.GetInt16(2).ToString(),
                            consulta.GetString(3),
                            consulta.GetString(4),
                            consulta.GetString(5),
                            consulta.GetString(6),
                        };
                        dgRes.Rows.Add(filas);
                    }
                }
                conectado1.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }

            string queryh = "select * from Huesped";
            MySqlConnection conectado5 = conectarBD();
            MySqlCommand comandoSQL4 = new MySqlCommand(queryh, conectado5);
            comandoSQL4.CommandTimeout = 60;
            try
            {
                conectado5.Open();
                MySqlDataReader consulta = comandoSQL4.ExecuteReader();
                //limpiar el grid
                dgDatos.Rows.Clear();
                dgDatos.Refresh();
                //validar si se encontraron datos
                if (consulta.HasRows)
                {
                    while (consulta.Read())
                    {
                        string[] filas = {
                            consulta.GetInt32(0).ToString(),
                            consulta.GetString(1),
                            consulta.GetString(2),
                            consulta.GetString(3),
                            consulta.GetString(4),
                        };
                        dgDatos.Rows.Add(filas);
                    }
                }
                conectado5.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            actualizar();
        }

        private void actualizar()
        {
            MySqlConnection conectado1 = conectarBD();
            string query = "update Huesped set nombre_huesped = '" + TxtNombre.Text + "', telefono = '" + TxtTelefono.Text + "', correo = '" + TxtCorreo.Text + "', origen_cliente = '" + TxtOrigen.Text + "' where id_huesped = " + TxtId.Text;
            MySqlCommand comandoSQL = new MySqlCommand(query, conectado1);
            comandoSQL.CommandTimeout = 60;
            try
            {
                conectado1.Open();
                MySqlDataReader result = comandoSQL.ExecuteReader();
                MessageBox.Show("Registro actualizado");
                consultaGen();
                //limpiar();
                conectado1.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error:" + err.ToString());
            }
            limpiar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            MySqlConnection conecta1 = conectarBD();
            string query = "delete from Reservacion where folio = '" + TxtFoil.Text + "'";
            DialogResult dialog0Result = MessageBox.Show("¿Confirma que desea eliminar?", "Advertencia", MessageBoxButtons.YesNo);
            if (dialog0Result == DialogResult.Yes)
            {
                MySqlCommand comando = new MySqlCommand(query, conecta1);
                comando.CommandTimeout = 60;
                try
                {
                    conecta1.Open();
                    MySqlDataReader result = comando.ExecuteReader();
                    consultaGen();
                    limpiar();
                    MessageBox.Show("Registro eliminado");
                    conecta1.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error: " + err.ToString());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MySqlConnection conectado1 = conectarBD();
            string query = "Select nombre_empleado from Empleado where id_empleado = " + comboEmp.Text + "";
            MySqlCommand comandoSQL1 = new MySqlCommand(query, conectado1);
            comandoSQL1.CommandTimeout = 60;
            try
            {
                conectado1.Open();
                MySqlDataReader result = comandoSQL1.ExecuteReader();
                //si existen datos
                if (result.HasRows)
                {
                    while (result.Read())
                    {
                        nombre.Text = result.GetString(0);
                    }
                }
                else
                {
                    MessageBox.Show("No se encuentra el registro");
                }
                conectado1.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error: " + err.ToString());
            }
        }
    }
}
